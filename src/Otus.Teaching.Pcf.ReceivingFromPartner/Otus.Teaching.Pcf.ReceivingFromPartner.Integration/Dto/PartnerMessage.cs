﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class PartnerMessage
    {
        public Guid? PartnerManagerId { get; set; }

    }
}
