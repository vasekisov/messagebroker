﻿using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers;
using System.Threading.Tasks;
namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface IGivingService
    {
            public Task GivePromoCodesToCustomersWithPreferenceAsync(GivingToCustomersMessage message);        

    }
}
