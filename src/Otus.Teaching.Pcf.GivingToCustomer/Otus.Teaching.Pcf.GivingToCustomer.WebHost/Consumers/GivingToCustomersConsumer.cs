﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivingToCustomersConsumer : IConsumer<GivingToCustomersMessage>
    {
        private readonly IGivingService _promoCodeService;
        public GivingToCustomersConsumer(IGivingService promoCodeService)
        {
            _promoCodeService = promoCodeService;
        }

        public async Task Consume(ConsumeContext<GivingToCustomersMessage> context)
        {
            await _promoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
