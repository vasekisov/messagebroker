﻿using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Conumers
{
    public class PartnerMessage
    {
        public Guid? PartnerManagerId { get; set; }
    }
}
