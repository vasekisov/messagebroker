﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Conumers
{
    public class PartnerPromoCodeConsumer : IConsumer<PartnerMessage>
    {
        private readonly IPartnerPromoCodeService _partnerPromoCodeService;
        public PartnerPromoCodeConsumer(IPartnerPromoCodeService partnerPromoCodeService)
        {
            _partnerPromoCodeService = partnerPromoCodeService;
        }

        public async Task Consume(ConsumeContext<PartnerMessage> context)
        {
            await _partnerPromoCodeService.UpdateAppliedPromocodesAsync(context.Message);
        }
    }
}
