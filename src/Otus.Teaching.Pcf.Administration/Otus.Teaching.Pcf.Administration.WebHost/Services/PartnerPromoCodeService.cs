﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Conumers;
using System.Threading.Tasks;
namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class PartnerPromoCodeService : IPartnerPromoCodeService
    {
        private readonly IRepository<Employee> _employeeRepository;
        public PartnerPromoCodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task UpdateAppliedPromocodesAsync(PartnerMessage message)
        {
            var employee = await _employeeRepository.GetByIdAsync(message.PartnerManagerId.Value);

            if (employee == null)
                throw new System.Exception("Manager not found");

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
