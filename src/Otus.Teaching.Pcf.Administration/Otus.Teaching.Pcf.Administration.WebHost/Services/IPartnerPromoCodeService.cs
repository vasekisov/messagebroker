﻿using Otus.Teaching.Pcf.Administration.WebHost.Conumers;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public interface IPartnerPromoCodeService
    {
        public Task UpdateAppliedPromocodesAsync(PartnerMessage message);
    }
}
